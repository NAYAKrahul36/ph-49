#include <stdio.h>
int input_array_size()
{
   int n;
   printf("Enter array size\n");
   scanf("%d",&n);
   return n;
}

void input_array(int n, int a[n])
{
   for(int i=0;i<n;i++)
   {
       printf("Enter the element a[%d]: ",i);
       scanf("%d",&a[i]);
    }
}

int distinct(int n, int a[n])
{
    int i,j,c=0;
	for(i=0;i<n;i++)
	{
		int j=0;
		for(j=0;j<i;j++)
			if(a[i]==a[j])
				break;
	        if(i==j)
				c++;
	}
				return c;
}

void output(int c)
{
   
    printf("%d\n",c);
}

int main()
{
   int n=input_array_size();
   int a[n];
   input_array(n,a);
   int d;
   d= distinct(n,a);
   output(d);
   return 0;
}


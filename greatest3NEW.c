#include <stdio.h>
int input()
{
    int in;
    scanf("%d",&in);
    return in;
}
int compute(int a,int b,int c)
{
    int large;
    large = a > b ? (a > c ? a : c) : (b > c ? b : c) ;
    return large;
}
void output(int max)
{
    printf("The largest number is %d",max);
}
int main(void)
{
    int a,b,c,large;
    printf("Enter the 3 numbers\n");
    a=input();
    b=input();
    c=input();
    large=compute(a,b,c);
    output(large);
    return 0;
}


//write a prog to merge two sorted arrays 
#include <stdio.h>
int input_array_size()
{
   int n;
   printf("Enter array size\n");
   scanf("%d",&n);
   return n;
}

void input_array(int n1,int n2,int a[n1],int b[n2])
{
   for(int i=0;i<n1;i++)
   {
       printf("Enter the element a[%d]: ",i);
       scanf("%d",&a[i]);
    }
	for(int i=0;i<n2;i++)
   {
       printf("Enter the element b[%d]: ",i);
       scanf("%d",&b[i]);
    }
}

int merge(int n1,int n2,int a[n1],int b[n2])
{
  int i=0,j=0,k=0,c[n1+n2]; 
  
    while (i<n1&&j<n2) 
    { 
       
        if (a[i]<b[j]) 
		{
            c[k++]=a[i++]; 
		}
        else
		{
            c[k++]=b[j++]; 
		}
    } 
  
    while(i<n1) 
	{
        c[k++]=a[i++]; 
	}
  
    while(j<n2) 
	{
        c[k++]=b[j++]; 
	}
		
		for(i=0;i<(n1+n2);i++)
		{
			printf("%d",c[i]);
		}
		return 0;
}

int main()
{
   int n1=input_array_size();
   int n2=input_array_size();
   int a[n1],b[n2];
   input_array(n1,n2,a,b);
   merge(n1,n2,a,b);
   return 0;
}



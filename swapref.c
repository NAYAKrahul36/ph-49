#include <stdio.h>
int swap_ref(int *,int *);
int main()
{
   int a,b;
   printf("enter 2 numbers to be swapped\n");
   scanf("%d %d",&a,&b);
   printf("number before swapping=%d %d\n",a,b);
   swap_ref(&a,&b);
   printf("number after swapping=%d %d",a,b);
   return 0;
   }
   int swap_ref(int *c,int *d)
   {
    int temp;
    temp=*c;
    *c=*d;
    *d=temp;
    return 0;
}
